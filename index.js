/*
import 'ol/ol.css';
import Feature from 'ol/Feature';
import Map from 'ol/Map';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON';
import Circle from 'ol/geom/Circle';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import {OSM, Vector as VectorSource} from 'ol/source';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
*/


var styles = {
  'LineString': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      lineDash: [4],
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)'
    })
  })
};

var styleFunction = function(feature) {
  return styles[feature.getGeometry().getType()];
};


var geojsonObject = {"type": "FeatureCollection",
  "features": [
      {"type": "LineString", "coordinates": [[263, 730], [263, 732], [263, 730]]},
      {"type": "LineString", "coordinates": [[307, 682], [306, 683], [306, 686], [307, 686], [307, 685], [308, 684], [309, 684], [309, 682], [307, 682]]},
      {"type": "LineString", "coordinates": [[305, 650], [305, 651], [306, 651], [305, 650]]},
      {"type": "LineString", "coordinates": [[594, 636], [594, 639], [596, 639], [596, 637], [595, 637], [594, 636]]},
      {"type": "LineString", "coordinates": [[184, 588], [184, 589], [185, 589], [185, 588], [184, 588]]},
      {"type": "LineString", "coordinates": [[204, 584], [203, 585], [203, 587], [204, 587], [205, 586], [205, 584], [204, 584]]},
      {"type": "LineString", "coordinates": [[230, 571], [229, 572], [230, 572], [230, 571]]},
      {"type": "LineString", "coordinates": [[587, 564], [587, 565], [587, 564]]},
      {"type": "LineString", "coordinates": [[512, 556], [512, 557], [513, 557], [513, 556], [512, 556]]},
      {"type": "LineString", "coordinates": [[199, 539], [196, 542], [197, 543], [199, 543], [201, 541], [201, 540], [200, 539], [199, 539]]},
      {"type": "LineString", "coordinates": [[26, 510], [25, 511], [26, 512], [27, 512], [28, 511], [27, 510], [26, 510]]},
      {"type": "LineString", "coordinates": [[310, 487], [310, 487]]},
      {"type": "LineString", "coordinates": [[502, 485], [501, 486], [501, 489], [502, 489], [503, 488], [503, 485], [502, 485]]},
      {"type": "LineString", "coordinates": [[541, 412], [540, 413], [541, 414], [543, 414], [544, 413], [545, 413], [546, 412], [541, 412]]},
      {"type": "LineString", "coordinates": [[536, 412], [537, 413], [538, 413], [538, 412], [536, 412]]},
      {"type": "LineString", "coordinates": [[398, 393], [397, 394], [396, 394], [395, 395], [393, 395], [391, 397], [391, 400], [388, 403], [388, 408], [391, 411], [391, 413], [392, 414], [393, 414], [394, 415], [394, 420], [396, 420], [397, 419], [397, 418], [400, 415], [402, 415], [403, 414], [404, 414], [404, 413], [406, 411], [406, 409], [407, 408], [407, 405], [409, 403], [410, 403], [411, 402], [411, 399], [406, 394], [406, 393], [398, 393]]},
      {"type": "LineString", "coordinates": [[690, 377], [692, 377], [690, 377]]},
      {"type": "LineString", "coordinates": [[523, 374], [522, 375], [517, 375], [517, 376], [518, 377], [518, 378], [519, 378], [519, 377], [520, 376], [521, 376], [522, 377], [523, 377], [524, 376], [524, 375], [525, 374], [523, 374]]},
      {"type": "LineString", "coordinates": [[173, 358], [172, 359], [174, 359], [173, 358]]},
      {"type": "LineString", "coordinates": [[274, 355], [274, 356], [275, 356], [275, 355], [274, 355]]},
      {"type": "LineString", "coordinates": [[293, 325], [293, 326], [293, 325]]},
      {"type": "LineString", "coordinates": [[299, 290], [299, 291], [301, 291], [301, 290], [299, 290]]},
      {"type": "LineString", "coordinates": [[570, 254], [571, 255], [572, 255], [572, 254], [570, 254]]},
      {"type": "LineString", "coordinates": [[275, 251], [275, 251]]},
      {"type": "LineString", "coordinates": [[493, 150], [493, 151], [492, 152], [493, 152], [495, 150], [493, 150]]},
      {"type": "LineString", "coordinates": [[526, 82], [525, 83], [525, 85], [526, 85], [527, 84], [527, 83], [526, 82]]}
    ]
  };

/*
var geojsonObject = {"type":"FeatureCollection", "features" : [{"type": "LineString", "coordinates": [[199, 539], [196, 542], [197, 543], [199, 543], [201, 541], [201, 540], [200, 539], [199, 539]] }]};
*/
features = [];
geojsonObject["features"].forEach(function(feature) {
  features.push(new ol.Feature( { geometry : new ol.geom.LineString(feature["coordinates"])}))});

var vectorSource = new ol.source.Vector({
  features: features
});

vectorSource.getFeatures().forEach(function(f){  console.log(f.getGeometry().getType())})


var vectorLayer = new ol.layer.Vector({
  source: vectorSource,
  style: styleFunction
});

var extent = [0, 0, 799, 799];
var projection = new ol.proj.Projection({
      code: 'local_image',
      units: 'pixels',
      extent: extent,
      worldExtent: [...extent]
    });

var map = new ol.Map({
  layers: [
    vectorLayer
  ],
  target: 'map',
  view: new ol.View({
    projection: projection,
    center: ol.extent.getCenter(extent),
    resolution: 800 / 512

  })
});
